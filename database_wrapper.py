import psycopg2
# -*- coding: utf-8 -*-
import os
from dotenv import load_dotenv
load_dotenv()

def exec_update_insert_command(cmd):
    conn = psycopg2.connect(host=os.getenv("HOST"), user=os.getenv("USER"), password=os.getenv("PASSWORD"), port=os.getenv("PORT"), database=os.getenv("DB_NAME"))
    f = open("log.txt", "w+")
    f.write(cmd)
    try:
        print(cmd)
        cursor = conn.cursor()
        cursor.execute(cmd)
        conn.commit()
        print('%s Updated' % db_name)
        status = "ok"
    except Exception as ex:
        status = str(ex)
    finally:
        if conn:
            cursor.close()
            conn.close()
            return status

def select_from_table(cmd):
    print(cmd)
    conn = psycopg2.connect(host=os.getenv("HOST"), user=os.getenv("USER"), password=os.getenv("PASSWORD"), port=os.getenv("PORT"), database=os.getenv("DB_NAME"))
    f = open("log.txt","w+")
    f.write(cmd)
    result = []
    try:
        cursor = conn.cursor()
        cursor.execute(cmd)
        result = cursor.fetchall()


    except Exception as ex:
        print(str(ex))
    finally:
        if conn:
            cursor.close()
            conn.close()
            print(result)
            return result


def start(DB, chat_id, username, name, sname, status):
    cmd = f"INSERT INTO public.users(chat_id, username, name, sname, status) VALUES ({chat_id},'{username}','{name}','{sname}',{int(status)});"
    print(cmd)
    status = exec_update_insert_command(cmd)
    return status


def isExist(DB, chat_id):
    result = -1
    try:
        cmd = f'SELECT chat_id from public.users WHERE chat_id = {chat_id}'
        result = select_from_table(cmd)[0][0]
    except Exception as ex:
        result = -1
    finally:
        return result


def pullIn(DB, student_id):
    cmd = f'INSERT INTO public.\"QuestionPull\"(student_id) VALUES({int(student_id)});'
    exec_update_insert_command(cmd)
    cmd = 'select COUNT(id) from public.\"QuestionPull\"'
    exec = select_from_table(cmd)
    print('%s Updated' % (DB))
    print(exec)
    return exec
#pullIn('bk',12443)


def get_row_by_userid(DB, table, chat_id):
    #exec = select_from_table(f'SELECT * FROM {table} WHERE chat_id = {chat_id}')

    if table == 'QuestionPull':
        _exec = select_from_table(f'SELECT * FROM {table} WHERE student_id = {chat_id}')

    else:
        _exec = select_from_table(f'SELECT * FROM {table} WHERE chat_id = {chat_id}')
    return _exec
#get_row_by_userid('-', "\"users\"",228)


def delete_from(DB, TABLE, WHAT, COND):
    cmd = f'DELETE FROM "{TABLE}" where {WHAT} = {COND}'
    exec_update_insert_command(cmd)
#delete_from('db','users','chat_id',228)


def get_data(DB, table, what, chat_id):
    cmd = ''
    if (table == 'QuestionPull'):
        cmd = f'SELECT {what} FROM "{table}" WHERE id = {chat_id}'
    elif (table == 'Instructors'):
        cmd = f'SELECT chat_id from Instructors where FIO = {str(chat_id)}'
    else:
        cmd = f'SELECT {what} FROM {table} WHERE chat_id = {chat_id}'
    return select_from_table(cmd)
#get_data('db','QuestionPull','student_id',2)


def get_data_prepod(DB, table, what, FIO):
    chat_id = select_from_table(f'SELECT chat_id from \"instructors\" where \"FIO\"= \'{FIO}\'')
    print(type(chat_id))
    if chat_id:
        print("Найдено")
        return str(chat_id)
    else:
        print('Эксперта нет в базе данных')
        return '0'

#print(get_data_prepod('-',"Instructors",'chat','Метрошина А. П.'))


def get_data_instructor(DB, table, chat_id):
    return select_from_table(f"SELECT * FROM \"{table}\" WHERE (instructor_id = {chat_id} or student_id = {chat_id}) and stream = {True}")



#get_data_instructor('s','QuestionPull', -1)


def insert_q(DB, text, keywords, src, body):##переделать так , чтобы макс при нуле работал
    exec_update_insert_command(f"INSERT INTO \"question\"(text, keywords, src, body) VALUES(\'{str(text)}\', \'{str(keywords)}\',\'{str(src)}\', \'{str(body)}\')")
    result_index_raw = select_from_table("select MAX(id) from \"question\"")
    print(result_index_raw)
    return int(result_index_raw[0][0])

#print(insert_q("-","ЭТО ТЕКСТ","СЛОВО КЛЮЧ","www.vk.com","ТЕЛО"))

def insert_q_json(DB, text, keywords, url):
    exec_update_insert_command(f"INSERT INTO \"question\"( text, keywords, src) VALUES( \'{str(text)}\', \'{str(keywords)}\',\'{str(url)}\')")
    result_index_raw = select_from_table("select MAX(id) from \"question\"")
    return int(result_index_raw[0][0]) + 1


def update_s(DB, table, where, what, id):  # add some str data value to user's database row
    if table == 'QuestionPull':
        print(str(id))
        cmd = f"UPDATE \"{table}\" SET {where} = \'{what}\' WHERE  student_id = {int(id)}"
        #cursor.execute('UPDATE %s SET %s = \'%s\' WHERE  student_id = %d' % (table, where, what, int(id)))
    else:
        cmd = f"UPDATE \"{table}\" SET {where} = \'{what}\' WHERE  chat_id = {int(id)}"
        #cursor.execute('UPDATE %s SET %s = \'%s\' WHERE  chat_id = %d' % (table, where, what, int(id)))

    exec_update_insert_command(cmd)


#update_s("db","QuestionPull","answer","ЖОПА", 12343)

def update_i(DB, table, where, what, id):  # add some str data value to user's database row
    if table == 'QuestionPull':
        print(str(id))
        cmd = f"UPDATE \"{table}\" SET {where} = {what} WHERE  student_id = {int(id)}"
        #cursor.execute('UPDATE %s SET %s = \'%s\' WHERE  student_id = %d' % (table, where, what, int(id)))
    else:
        cmd = f"UPDATE \"{table}\" SET {where} = {int(what)} WHERE  chat_id = {int(id)}"
        #cursor.execute('UPDATE %s SET %s = \'%s\' WHERE  chat_id = %d' % (table, where, what, int(id)))

    exec_update_insert_command(cmd)

#update_i("db","QuestionPull","instructor_id",22333, 12343)


def toSqlDataFormat(string):
    print(string)
    dataForm = '20' + string[6] + string[7] + '-' + string[3] + string[4] + '-' + string[0] + string[1]
    print(dataForm)
    return dataForm

def getPeriodQuestionByData(DB, startData, endData):
    return select_from_table(f"SELECT * FROM \"question\" WHERE date BETWEEN \'{toSqlDataFormat(startData)}\' and \'{toSqlDataFormat(endData)}\'")

def getQuestionByData(DB, Data):
   return select_from_table(f"SELECT * FROM \"questio\" WHERE date = \'{toSqlDataFormat(Data)}\'")

#getQuestionByData('-','02-05-22')


def getKeywordsRows(kwString):
    kwArray = kwString.split()
    if len(kwArray) > 0:
        cast = 'select * from question where ( keywords like \'' + kwArray[0] + ' %\' OR keywords like \'% ' + kwArray[0] + ' %\' OR keywords like \'% ' + kwArray[0] + '\')'
        if len(kwArray) > 1:
            for kw in kwArray[1:]:
                cast = cast + ' AND ( keywords like \'' + kw + ' %\' OR keywords like \'% ' + kw + ' %\' OR keywords like \'% ' + kw + '\')'
    return select_from_table(cast)
#getKeywordsRows('СЛОВО КЛЮЧ')


def getAdrresToEchoTo(chat_id):
    return select_from_table(f'select * from \"QuestionPull\" where (instructor_id = {chat_id} or student_id = {chat_id}) and stream = True')


def pushMessageInHistory(chat_id, text):
    exec_update_insert_command(f"UPDATE  \"QuestionPull\" SET answer = answer || \'{text}\'  || \'\n\' WHERE  student_id = {int(chat_id)}")

# DB = 'BK536'
# table = 'users'
# print(get_all_data(DB,table))
# update_s(DB, table,'username','@Frugtoy','111')
# print(get_all_data(DB,table))

# print(toSqlDataFormat('22.06.21'))

# conn = sqlite3.connect('BK536')
# cursor = conn.cursor()
# cursor.execute('select * from question where date between ' + '\''+ toSqlDataFormat('22.06.00') + '\'' +  ' and '+ '\'' + toSqlDataFormat('22.06.42')+ '\'')
# exec = cursor.fetchall()
# cursor.close()
# conn.close()
# print(exec)
# print(getKeywordsRows('ГДЕ КОГДА'))'''
#print(select_from_table('select * from \"QuestionPull\"'))
#insert_q(' ', "test", "test","vk.com","sipkh")
