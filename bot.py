# Таблицы базы данных: Users, QuestionPull, Log, PrepodsPull
# Users: chat_id, userStatus, Qdate, Qtext, keyWords, 
#          Некая машина состояний + побочные данные из которых мы формируем вторую таблицу на этапе удовлетворения Юзера ответом бота) 
# Log: нашли ли мы вопросы в истории (если да то какие), статьи какие нашли и порекомендовали, каких преподов нашли, кому отправили, кто взял вопрос, кто отдал вопрос.
# QuestionPull: Вопрос пользователя, Удовлетворивший ответ (текст : ссылка на статью), дата
# PrepodsPull: ФИО, Чат айди

import os
from aiogram.types.message import Message
import request_worker as vl
#import sqlwrapper as sw
import database_wrapper as sw
import check
from aiogram import Bot, Dispatcher, executor, types
import json
from dotenv import load_dotenv

load_dotenv()
API_TOKEN = os.getenv("API_TOKEN")
bot = Bot(token=API_TOKEN)
dp = Dispatcher(bot)
ADMIN = '438662734'
PREPODS_CHAT = -1001748061956

#DATA BASE#################
DB = 'BK536'
UsersTable = 'users'
LogTable = 'log'
QuestionPull = 'QuestionPull'
PrepodsPull = 'prepodspull'
###########################

########USER_STATUS########
DEFAULT        = 0
QUESTION_INPUT = 1
BDSEARCH        = 2
KEYWORD_SEARCH  = 3
PREPOD_SEARCH   = 4
PREPOD_CHAT = 5
TUNNEL = 6
ENDTUNNEL = 7
###########################


def startKeyboard():
    markup = types.ReplyKeyboardMarkup(row_width=1 , resize_keyboard=True)
    item_btn1 = types.KeyboardButton('Задать вопрос')
    item_btn2 = types.KeyboardButton('Описание бота')
    item_btn3 = types.KeyboardButton('Обратная связь')
    markup.add(item_btn1,item_btn2, item_btn3)
    return(markup)


#Логика
#Для теста - удалить
@dp.message_handler(commands = ['default'])
async def default_message(message: types.Message):
    await bot.send_message(message.chat.id,'Статус сброшен до DEFAULT')
    sw.update_i(DB,'users','status',DEFAULT,message.chat.id)


#Start --> если пользователь новый создаем строку в USERS со статусом  DEFAULT (последние три поля будут привязаны к конкретному вопросу, формируются на пункте 2),Если статус  DEFAULT добавляем кнопку - Задать вопрос на панель
@dp.message_handler(commands = ['start'])
async def start_message(message: types.Message):

    if( message.chat.type != 'supergroup'):
        status = sw.start(DB,message.chat.id,message.chat.username,message.chat.first_name,message.chat.last_name,DEFAULT)
        err = 'ОШИБКА:  повторяющееся значение ключа нарушает ограничение уникальности \"users_pkey\"'
        if(status =='ok'):
            await bot.send_message(message.chat.id,text = 'Привет кожаный, хочешь узнать меня поближе?\nЖми смелее на мои кнопки!',reply_markup = startKeyboard())
        elif status[0:len(err)] == err :#'UNIQUE constraint failed: users.chat_id'):
            await bot.send_message(message.chat.id,text = 'Кажется мы раньше встречались',reply_markup = startKeyboard())
        else:
            await bot.send_message(message.chat.id,text = status,reply_markup = startKeyboard())
    else:
            await bot.send_message(message.chat.id,'Отправьте мне сообщение в ЛС, чтобы я мог открывать чат со студентами')


#Конец общения студента и преподавателя по иннициативе одного из двух
@dp.message_handler(commands= ['end'])#
async def end_messge(message: types.Message):
    if(sw.get_data(DB,'users','status',message.chat.id)[0][0] == TUNNEL):
        try:
            QuestionPullRaw = sw.get_data_instructor(DB,QuestionPull,message.chat.id) #получаем данные вопроса Студента
            UsersTableRaw = sw.get_row_by_userid(DB, UsersTable, QuestionPullRaw[0][1])#а так же 'мясо' вопроса
            print('ТАБЛИЦА ВОПРОСОВ:\n', QuestionPullRaw, 'Таблица пользователей\n', UsersTableRaw)
            student_id = QuestionPullRaw[0][1]
            instructor_id = QuestionPullRaw[0][3]
            sw.update_i(DB,'users','status',ENDTUNNEL,student_id)
            sw.update_i(DB,'users','status',DEFAULT,instructor_id)
            sw.update_i(DB,'QuestionPull', 'stream', False, student_id)
            src = 'https://www.google.com/search?q='
            try:
                for word in UsersTableRaw[0][5].split():
                    src = src + word + '+'

            except:
                src = src + UsersTableRaw[0][5]

            print(src)
            num = sw.insert_q( DB, UsersTableRaw[0][5], UsersTableRaw[0][6], src, QuestionPullRaw[0][2] )
            keyboard_how = types.InlineKeyboardMarkup(row_width=1)
            first_button = types.InlineKeyboardButton("Помогло", callback_data='Y' + ' ' + str(num) + ' ' + str(instructor_id))
            second_button = types.InlineKeyboardButton("Не помогло", callback_data='N' + ' ' + str(num))
            keyboard_how.add(first_button, second_button)
            await bot.send_message(student_id, text="Эксперт завершил диалог:\n\n"+ QuestionPullRaw[0][2], reply_markup=startKeyboard())
            await bot.send_message(student_id, text="Удовлетворены ли вы ответом?", reply_markup= keyboard_how)
            await bot.send_message(instructor_id,text="Студент завершил диалог:\n\n"+ QuestionPullRaw[0][2], reply_markup=startKeyboard())
        except Exception as ex:
            print(str(ex))
            await bot.send_message(message.chat.id, text="У вас нет активных чатов")
    else:
        await bot.send_message(message.chat.id, text="У вас нет активных чатов")


#1)Кнопка 'Задать вопрос' --> при нажатии меняем статус конкретного пользователя на QUESTION_INPUT
@dp.message_handler()
async def text_message(message: types.Message):
    if message.text == "Описание бота":
        await bot.send_message(message.chat.id, '''Бот помогает в поисках ответа на технические вопросы по кафедральным предметам.\n
·Если ответы бота вас не удовлетворили, бот передаст сообщение нужному эксперту, с которым вы сможете общаться через бота в любой момент.''',reply_markup = startKeyboard())

    elif message.text == "Обратная связь":
        await bot.send_message(message.chat.id,'''Контакты:\nБорис Лесин\ntelegram: @Frugtoy\nemail: borislesin@yandex.ru''',reply_markup = startKeyboard())

    elif message.text == 'Задать вопрос' and sw.get_data(DB,'users','status',message.chat.id)[0][0] == DEFAULT:
        await bot.send_message(message.chat.id, 'Напишите свой вопрос боту')
        sw.update_i(DB, 'users', 'status', QUESTION_INPUT, message.chat.id)


#2)Если получили сообщение и статус QUESTION_INPUT --> отправление message.text в сервис на разбор по ключевым словам, сохранение в базе данных полей date, question_text, key_words(на данном этапе), изменяем статус на BDSEARCH
    elif sw.get_data(DB, 'users', 'status', message.chat.id)[0][0] == QUESTION_INPUT or sw.get_data(DB, 'users', 'status', message.chat.id)[0][0] == DEFAULT:
        await bot.send_message(message.chat.id,'Ищу в ранее заданных вопросах')
        sw.update_i(DB,'users','status',BDSEARCH,message.chat.id)
        sw.update_s(DB,'users','question_text', message.text,message.chat.id)
        sw.pullIn(DB, message.chat.id)
        path = './users/' + str(message.chat.id)
        try:
            os.mkdir(path)
        except:
            print("dir is already exist")
        finally:
            #3)Отправляем ключевые слова в БД, Если ключевое слово совпало, отправляем значение юрла пользователю, каждый с кнопкой 'ссылка на статью', в конце спрашиваем удовлетворил ли ответ пользователя(да/нет), если да -> меняем статус пользователя на DEFAULT,
            print('in finally')
            try:
                key_words = vl.getKeywords(message.text)
                sw.update_s(DB,'users','keywords', key_words, message.chat.id)
                bd_compare_matches = sw.getKeywordsRows(key_words)
                matches_count = len(bd_compare_matches)
                print(len(bd_compare_matches))
                if matches_count > 0:
                    for answer in bd_compare_matches:
                        keyboard = types.InlineKeyboardMarkup(row_width=1)
                        first_button = types.InlineKeyboardButton("Ссылка", url = answer[3])
                        keyboard.add(first_button)
                        await bot.send_message(message.chat.id, answer[2] +'\n\n'+answer[5],parse_mode="Markdown",reply_markup = keyboard)

                    keyboard_how = types.InlineKeyboardMarkup(row_width=1)
                    first_button = types.InlineKeyboardButton("Помогла", callback_data='helped')
                    second_button = types.InlineKeyboardButton("Не помогла", callback_data='!helped')
                    keyboard_how.add(first_button, second_button)
                    await bot.send_message(message.chat.id, "Помогла ли вам информация?", reply_markup = keyboard_how)

                #Cоставление массива строк для поискового файла json
                #Если нет, или база данных пуста --> делаем статус KEYWORD_SEARCH ( в базе данных LOG ,в поле найденные вопросы в бд - ставим: не найдено)

                else:
                    keyboard = types.InlineKeyboardMarkup(row_width = 1)
                    keyboard.add(types.InlineKeyboardButton( text = 'Да', callback_data ='!helped'))
                    keyboard.add(types.InlineKeyboardButton( text = 'Нет', callback_data ='SetDefaultStatus'))
                    await bot.send_message(message.chat.id, "Не удалось найти информацию в истории вопросов.\n\nВыполнить поиск по базе данных?", reply_markup = keyboard)

            except Exception as ex:
                sw.update_i(DB,'users','status',DEFAULT,message.chat.id)
                print(ex)
                await bot.send_message(message.chat.id, 'Не удалось выделить ключевые слова по вашему вопросу, пожалуйста перефразируйте его. ')# + str(ex))

    elif len(sw.getAdrresToEchoTo(message.chat.id)) >= 1:
        row = sw.getAdrresToEchoTo(message.chat.id)
        print(row)
        if message.chat.id == row[0][1]:
            await bot.send_message(row[0][3], message.text)
            sw.pushMessageInHistory(row[0][1], '[student]: ' + message.text +'\n\n' )
        else:
            await bot.send_message(row[0][1], message.text)
            sw.pushMessageInHistory(row[0][1], '[expert]: ' + message.text + '\n\n' )


@dp.callback_query_handler()
async def callback_inline(call: types.CallbackQuery):
    print(call.data)
#4)Если статус BDSEARCH и call.data == !helped
#       Меняем статус на KEYWORD_SEARCH идем в сервис по ключевым словам, получаем ответ из статей,
#       Если найдено - отправляем ответ, в каждом их которых прикрепляем кнопки "ссылка"  и "ответ удовлетворил", так же в конце отправляем кнопку "Ответы не удовлетворили"
#           Ответы не удовлетворили  - редактируем все ответы, убирая кнопки "ответ удовлетворил", меняем статус пользователя на PREPOD_SEARCH
#           Ответ удовлетворил -  редактируем все ответы, добавляем удовлетворивший ответ в базу данных по ключевым словам, меняем статус пользователя на DEFAULT
    if call.data == "!helped":
        path = './users/' + str(call.message.chat.id)
        json_key_words = ''
        if sw.get_data(DB, 'users', 'status', call.message.chat.id)[0][0] == BDSEARCH:

            sw.update_i(DB, 'users', 'status', KEYWORD_SEARCH, call.message.chat.id)

            key_words = sw.get_data(DB, 'users', 'keywords', call.message.chat.id)[0][0]
            await bot.edit_message_text(chat_id=call.message.chat.id,message_id=call.message.message_id, text='Отправляю запрос в базу данных')
            for i in key_words.split():
                json_key_words = json_key_words + "\""+ i + '\",'
            vl.setJson_(json_key_words[0:len(json_key_words)-1],path + '/')

            vl.search(path )
            with open(path + '/data.json', 'r', encoding='utf-8-sig') as f:
                txt = json.load(f)
                #print(txt)
                if txt["total-count"]== "0":
                            feedback_keyboard = types.InlineKeyboardMarkup(row_width = 1)
                            feedback_keyboard.add(types.InlineKeyboardButton('Нет', callback_data='SetDefaultStatus'))
                            feedback_keyboard.add(types.InlineKeyboardButton('Да', callback_data='!helped'))
                            await bot.send_message(call.message.chat.id , "Не удалось найти информацию в базе данных, связать вас с экспертом?" , reply_markup= feedback_keyboard)

                else:
                    try:
                        #index_in_json = 0
                        for i in txt["result-docs"]:
                            info_keyboard = types.InlineKeyboardMarkup(row_width = 1)
                            #await bot.send_message(call.message.chat.id, i["url"], reply_markup=info_keyboard)
                            button = types.InlineKeyboardButton("Ссылка",url = i['url'])
                            info_keyboard.add(types.InlineKeyboardButton(text="Помогло", callback_data='JSONhelped blank'))# + str(i['url'])))#'KW_HELPED '+ str(i['title'])+' '+ str(i['url'])) )
                            info_keyboard.add(button)
                            #i["description-header"]
                            #i["description-body"]
                            await bot.send_message(call.message.chat.id, '*'+i["description-header"]+'*' + ' ' + i["description-body"] + '\n' + i["url"], parse_mode="Markdown", reply_markup=info_keyboard)
                            #index_in_json = index_in_json + 1

                        feedback_keyboard = types.InlineKeyboardMarkup(row_width = 1)
                        feedback_keyboard.add(types.InlineKeyboardButton('Да', callback_data= 'helped'))
                        feedback_keyboard.add(types.InlineKeyboardButton('Нет, нужна консультация эксперта ', callback_data= '!helped'))

                        await bot.send_message(call.message.chat.id , "Вопрос решился?" , reply_markup= feedback_keyboard)
                    except Exception as e:
                        await bot.send_message(call.message.chat.id, str(e))
                        print(e)


#5)Если PREPOD_SEARCH --> идем в сервис по ключевым словам и преподам, получаем ответ из списка преподов, отправляем пулл преподов пользователю, под каждым кнопка 'отправить вопрос'
#   Пользователь выбирает препода и нажимает 'Отправить вопрос', бот по PrepodsPull определяет преподавателя отправляет вопрос с кнопками 'Ответить на вопрос' и 'Отдать вопрос'
#   Ответить на вопрос - включается,тело вопроса редактируется, убираются кнопки,включается статус RESPONSE_INPUT, преподаватель пишет ответ, при желании прикрепляя ссылку на статью например:
#       по теме ООП есть очень полезная статья [url: www.example.com], при отправке Бот прикрепляет юрл к кнопке, и возвращает сообщение пользователю, задавшему вопрос, с кнопками 'ссылка на статью' 'удовлетворило' 'не удовлетворило'
#           Удовлетворило    - редакт   ируем сообщение бота, убирая кнопки удовлетворило, не удовлетворило, сохраняем ответ в базе данных QuestionPull с текущей датой, ключевым словом, текстом ответа и ссылкой на статью(если она есть), меняем статус пользователя на DEFAULT, подчищаем побочные данные
#           Не удовлетворило - редактируем сообщение бота, убирая кнопки удовлетворило, не удовлетворило, вопрос отправляется в общий чат с преподавателями с кнопкой 'Беру на себя'.
#               При нажатии кнопки 'Беру на себя' - удаляем сообщение от бота, в чате преподов, отправляем его преподавателю, далее повторение логики Ответить на вопрос и Отдать вопрос пока не Ответ не удовлетворит.
        elif(sw.get_data(DB,'users','status',call.message.chat.id)[0][0] == KEYWORD_SEARCH):
            sw.update_i(DB,'users','status',PREPOD_SEARCH,call.message.chat.id)
            key_words = sw.get_data(DB, 'users','keywords',call.message.chat.id)[0][0]
            await bot.edit_message_text(chat_id=call.message.chat.id,message_id=call.message.message_id, text = 'Ищу экспертов по этой теме')
            for i in key_words.split():
                json_key_words = json_key_words + "\""+ i + '\",'
            vl.setPrepodJson(json_key_words[0:len(json_key_words)-1],path + '/')
            vl.search(path)
            with open(path +'/data.json','r',encoding='utf-8-sig') as f:
                text = json.load(f)
                print("ЗАГРУЖЕНО")

                try:
                    if text["total-count"] == "0":
                         question_body = sw.get_row_by_userid(DB,'users',call.message.chat.id)
                         print(question_body)
                         await bot.send_message(call.message.chat.id,text = 'Не нашел экспертов, отправляю вопрос в общий чат') #отпправить вопрос в общий чат
                         keyboard = types.InlineKeyboardMarkup(row_width = 1)
                         button = types.InlineKeyboardButton("беру на себя",callback_data = 'question '+str(question_body[0][0])) # заменить на поиск по иннициалам в бд преподов
                         keyboard.add(button)
                         sw.pullIn(DB,question_body[0][0])
                         sw.update_i(DB,'users','status',PREPOD_CHAT,call.message.chat.id)
                         await bot.send_message(PREPODS_CHAT,text = question_body[0][5],reply_markup = keyboard)
                    else:
                        data = set()
                        keyboard = types.InlineKeyboardMarkup(row_width = 1)
                        prepod = ''

                        for i in text["result-docs"]:
                            print(data)
                            try:
                                prepod = str(sw.get_data_prepod(DB,'instructors','chat_id',i['title'].strip())[0][0])
                                if prepod != '0':
                                    data.add(i['title'].strip())
                            except Exception as e:
                                print(e)
                            print('Вернувшийся чатайди:', prepod)

                        for expert in data:
                            button = types.InlineKeyboardButton(expert,callback_data = 'send_to ' +  expert)
                            keyboard.add(button)

                        if len(data) != 0:
                            await bot.send_message(call.message.chat.id,'Найдено экспертов: '+ str(len(data)) + '\n\nКому отправим вопрос?',reply_markup=keyboard)
                        else:
                            question_body = sw.get_row_by_userid(DB, 'users', call.message.chat.id)
                            print(question_body)
                            await bot.send_message(call.message.chat.id,text='Вернувшихся экспертов нет в моей базе данных, отправляю вопрос в общий чат')  # отпправить вопрос в общий чат
                            keyboard = types.InlineKeyboardMarkup(row_width=1)
                            button = types.InlineKeyboardButton("беру на себя", callback_data='question ' + str(question_body[0][0]))  # заменить на поиск по иннициалам в бд преподов
                            keyboard.add(button)
                            sw.pullIn(DB, question_body[0][0])
                            sw.update_i(DB, 'users', 'status', PREPOD_CHAT, call.message.chat.id)
                            await bot.send_message(PREPODS_CHAT, text=question_body[0][5], reply_markup=keyboard)

                except Exception as e:
                    print(e)


    #Студент выбрал преподователя из поиска сервиса Веги
    elif call.data.split()[0] =='send_to':
        print("call data: ", call.data)
        sw.update_i(DB,'QuestionPull','instructor_id', int(call.data.split()[1]),int(call.from_user.id))
        #await bot.edit_message_text(call.message.from_user.id, call.message.text +'\nВопрос взял на себя:'+ call.message.from_user.username)
        keyboard = types.InlineKeyboardMarkup(row_width = 1)
        first_button = types.InlineKeyboardButton('Вернуть',callback_data = 'dis ' + str(call.message.chat.id))
        second_button = types.InlineKeyboardButton('Ответить на вопрос', callback_data= "ans " + str(call.message.chat.id))
        keyboard.add(first_button, second_button)
        await bot.edit_message_text(chat_id=call.message.chat.id,message_id=call.message.message_id, text = 'Отправляю вопрос выбранному эксперту...')
        try:
            await bot.send_message(call.data.split()[1],text = sw.get_data(DB,'USERS','question_text',int(call.message.chat.id))[0][0], reply_markup= keyboard)
        except Exception as ex:
            print(str(ex))
            await bot.send_message(call.message.chat.id,text = 'Ошибка при отправке сообщения эксперту, убедитесь что он есть в базе данных, Можете повторно задать вопрос')
            sw.update_i(DB,'users','status', DEFAULT, call.from_user.id)


    #Помогло на этапе поиска по базе данных бота
    elif call.data.split()[0] == 'helped':
        sw.update_i(DB, 'users', 'status', DEFAULT, call.from_user.id)
        await bot.edit_message_text(chat_id=call.message.chat.id, message_id=call.message.message_id, text='Рад что смог вам помочь!\n Нажмите на кнопку \"Задать вопрос\"  как только захотите узнать ещё что-нибудь)')


    #Помогли статьи по ключевым словам на Веге
    elif call.data.split()[0] == 'JSONhelped':
        print(call.data)
        path = 'users/'+str(call.message.chat.id)
        with open(path +'/data.json','r',encoding='utf-8-sig') as f:
            #text = json.load(f)
            #print(text['result-docs'][0]['title'])
            UsersTableRaw = sw.get_row_by_userid(DB, UsersTable, call.message.chat.id)
            #print(text["result-docs"][int(call.data.split()[1])]['title'])
            #print(text["result-docs"][int(call.data.split()[1])]['url'])
            sw.insert_q_json( DB, call.message.text.split('\n')[0], UsersTableRaw[0][6], call.message.text.split('\n')[1])
        await bot.edit_message_text(chat_id=call.message.chat.id,message_id=call.message.message_id, text = call.message.text + '\n\nОтвет сохранён в истоии запросов')


    #Нажатие клавиши 'Беру на себя'
    elif call.data.split()[0] == 'question':
        print("call data: ", call.data)
        sw.update_i(DB,'QuestionPull','instructor_id',call.from_user.id, int(call.data.split()[1]))
        #await bot.edit_message_text(call.message.from_user.id, call.message.text +'\nВопрос взял на себя:'+ call.message.from_user.username)
        print(call.data.split()[1])
        keyboard = types.InlineKeyboardMarkup(row_width = 1)
        first_button = types.InlineKeyboardButton('Вернуть',callback_data = 'dis ' + call.data.split()[1])
        second_button = types.InlineKeyboardButton('Ответить на вопрос', callback_data= "ans " + call.data.split()[1])
        keyboard.add(first_button, second_button)
        await bot.send_message(call.from_user.id,text = call.message.text, reply_markup= keyboard)#, int(sw.get_data(DB,'QuestionPull','student_id',int(call.data.split()[1]))[0][0]))
        await bot.edit_message_text(chat_id=call.message.chat.id,message_id=call.message.message_id, text = call.message.text + '\n' + '🔁 Данный вопрос взяли 🔁' )


    #Нажатие клавиши 'Вернуть вопрос'
    elif call.data.split()[0] == 'dis':
        print("call data: ", call.data)
        sw.update_i(DB,'QuestionPull','instructor_id',-1, int(call.data.split()[1]))
        #await bot.edit_message_text(call.message.from_user.id, call.message.text +'\nВопрос взял на себя:'+ call.message.from_user.username)
        keyboard = types.InlineKeyboardMarkup(row_width = 1)
        button = types.InlineKeyboardButton("беру на себя",callback_data = 'question '+ call.data.split()[1]) # заменить на поиск по иннициалам в бд преподов
        keyboard.add(button)
        await bot.send_message(PREPODS_CHAT,text = call.message.text, reply_markup= keyboard)#, int(sw.get_data(DB,'QuestionPull','student_id',int(call.data.split()[1]))[0][0]))
        await bot.edit_message_text(chat_id=call.message.chat.id,message_id=call.message.message_id, text = call.message.text + '\n' + '🔁 Вопрос вернули в общий чат 🔁' )


    #Нажатие 'клавиши ответить на вопрос'
    elif call.data.split()[0] == 'ans':
        if( sw.get_data(DB,'users','status',call.message.chat.id)[0][0] != TUNNEL):
            sw.update_i(DB,'users','status',TUNNEL, call.data.split()[1])
            sw.update_i(DB,'users','status',TUNNEL, call.message.chat.id)
            sw.update_i(DB, 'QuestionPull','stream',True, int(call.data.split()[1]))
            keyboard_end = types.ReplyKeyboardMarkup(row_width=1 , resize_keyboard = True)
            item_btn1 = types.KeyboardButton('/end')
            keyboard_end.add(item_btn1)

            await bot.send_message(call.data.split()[1], text = 'Чат по вопросу \"' + call.message.text + '\" будет происходить через бота\nДля завершения нажмите /end', reply_markup= keyboard_end )
            await bot.edit_message_text(chat_id=call.message.chat.id,message_id=call.message.message_id, text = 'Начинаю чат со студентом' )
            await bot.send_message(call.message.chat.id, text = 'Чат по вопросу \"' + call.message.text + '\" будет происходить через бота\nДля завершения нажмите /end', reply_markup= keyboard_end)
        #Если препод нажал на ответить на вопрос и на нем уже висит чат:
        else:
             await bot.send_message(call.message.chat.id,'Для начала завершите текущий ответ на вопрос, напишите боту /end')


    #Нажатие на 'Не помогло' после /end
    elif call.data.split()[0] == 'N':
        print('N: ',call.data.split()[1])
        #sw.get_row_by_userid()
        sw.update_i(DB,'users','status',PREPOD_CHAT,call.from_user.id)
        sw.update_i(DB,'QuestionPull','instructor_id',-1, call.from_user.id)
        sw.update_s(DB, 'QuestionPull','answer',' ', call.from_user.id)

        #УДАЛИТЬ вопрост (его айди лежит в call.data.split()[1])
        sw.delete_from(DB,'question','id',int(call.data.split()[1]))
        #DELETE FROM question where id = 7
        keyboard = types.InlineKeyboardMarkup(row_width = 1)
        button = types.InlineKeyboardButton("беру на себя",callback_data = 'question '+str(call.from_user.id))
        keyboard.add(button)
        question = sw.get_data(DB, 'users', 'question_text',call.message.chat.id)[0][0]
        await bot.send_message(PREPODS_CHAT,text = question, reply_markup = keyboard)#, int(sw.get_data(DB,'QuestionPull','student_id',int(call.data.split()[1]))[0][0]))
        await bot.edit_message_text(chat_id=call.message.chat.id,message_id=call.message.message_id, text = question + '\n' + '🔁 Вопрос вернули в общий чат 🔁' )


    #Нажатие на 'Помогло' после /end
    elif call.data.split()[0] == 'Y':
        sw.update_i(DB,'users','status',DEFAULT,call.from_user.id)
        sw.delete_from(DB,'QuestionPull','student_id', call.message.chat.id)
        keyboard = types.InlineKeyboardMarkup(row_width = 1)
        button = types.InlineKeyboardButton("Сохранить",callback_data = 'SAVE + ' + call.data.split()[1]) # заменить на поиск по иннициалам в бд преподов
        s_button = types.InlineKeyboardButton("Не сохранять",callback_data = 'SAVE - ' + call.data.split()[1])
        keyboard.add(button, s_button)
        await bot.edit_message_text(chat_id=call.message.chat.id,message_id=call.message.message_id, text = 'Рад был помочь' )
        await bot.send_message(chat_id=call.data.split()[2], text = 'Могу я сохранить вашу историю переписки в своей базе данных?',  reply_markup= keyboard )


    #Логика Сохранить / Не сохранять
    elif call.data.split()[0] == 'SAVE':
        if call.data.split()[1] == '+': #Сохранить
             await bot.edit_message_text(chat_id=call.message.chat.id,message_id=call.message.message_id, text = 'Успешно сохранено')

        else:#Не сохранять
             sw.delete_from(DB,'question','id',int(call.data.split()[2]))
             await bot.edit_message_text(chat_id=call.message.chat.id,message_id=call.message.message_id, text = 'Уважаю ваш выбор')

    elif call.data == 'SetDefaultStatus':
        sw.update_i(DB,'users','status',DEFAULT,call.from_user.id)
        await bot.edit_message_text(chat_id=call.message.chat.id,message_id=call.message.message_id, text = 'Можете задавать следующий вопрос' )



async def alive_pulse(message: types.Message):
    await bot.send_message(ADMIN,"я ожил")


async def death_pulse(message: types.Message):
    await bot.send_message(ADMIN,"я умер")

'''Некоторые моменты'''
# !!!Важно, чтобы преподаватель не удалял вопрос, потому что в таком случае статус не поменяется на DEFAULT, что приведет к вечному зависанию пользователя на этапе PREPOD_SEARCH
# Можно попробовать решить этот вопрос c помощью добавления специальной команды бота '/status' при нажатии на которую бот проверит чат на наличие конкретного сообщения по  chat.message.message.id
#   Если существует - отправляем юзеру, что не о чем волноваться
#   Если не существует - отправляем юзеру, что его статус заменен на DEFAULT по причине того, что сообщение удалено безвозвратно        



#getKeywords("Язык")
#search('searchL.json')
#set_messaes_from_json('data.json')

executor.start_polling(dp, skip_updates=True, on_startup=alive_pulse,on_shutdown=death_pulse)


#Тест вопросы:
#1)"Bash скрипты на Linux" -> нет в базе, на сервисе keywords есть, преподавателей по вопросу - нет.
#2) Что делать, если я оладушек -> нет в базе, на сервисе k-w есть, статей нет.