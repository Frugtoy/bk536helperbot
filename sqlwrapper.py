from logging import exception
import sqlite3
import time 
from aiogram.types import chat

from attr import dataclass


def start(DB,chat_id,username,name, sname,status):
    try:
        conn = sqlite3.connect(DB)
        cursor = conn.cursor()
        cursor.execute('INSERT INTO users (chat_id ,username, name, sname, status)VALUES(?,?,?,?,?)', (int(chat_id),username,name,sname,int(status)))
        conn.commit()
        cursor.close()
        conn.close()
        print('%s Updated'%(DB))
        return("ok")
    except Exception as ex:
        return(str(ex))

def isExist(DB, chat_id):
    result = 0
    try:
        conn = sqlite3.connect(DB)
        cursor = conn.cursor()
        cursor.execute('select chat_id from users WHERE chat_id = %d'%(chat_id))
        exec = cursor.fetchall()
        cursor.close()
        conn.close()
        result = exec[0][0]
    except Exception as ex:
        result = -1
    return result
    
def pullIn(DB,student_id):
    try:
        conn = sqlite3.connect(DB)
        cursor = conn.cursor()
        cursor.execute('INSERT INTO QuestionPull(student_id) VALUES(%d);'%(int(student_id)))
        conn.commit()
        cursor.execute('select COUNT(id) from QuestionPull')
        exec = cursor.fetchall()
        cursor.close()
        conn.close()
        print('%s Updated'%(DB))
        print(exec)
        return exec
    except Exception as ex:
        print((str(ex)))

def get_all_data(DB,table):
    conn = sqlite3.connect(DB)
    cursor = conn.cursor()
    cursor.execute('SELECT %s FROM %s'%('*', table))
    exec = cursor.fetchall()
    cursor.close()
    conn.close()
    return exec
def get_row_by_userid(DB,table,chat_id):
    conn = sqlite3.connect(DB)
    cursor = conn.cursor()
    if table == 'QuestionPull':
        cursor.execute('SELECT * FROM %s WHERE student_id = %d'%(table, chat_id))
    else:
        cursor.execute('SELECT * FROM %s WHERE chat_id = %d'%(table, chat_id))
    exec = cursor.fetchall()
    cursor.close()
    conn.close()
    print(exec)
    return exec
def delete_from(DB,TABLE, WHAT, COND):
    try:
        conn = sqlite3.connect(DB)
        cursor = conn.cursor()
        cursor.execute('DELETE FROM %s where %s = %d'%(TABLE,WHAT,COND))
        conn.commit()
        cursor.close()
        conn.close()
        print('%s DELITED'%(DB))
        
    except Exception as ex:
        return(str(ex))

def get_data(DB,table,what, chat_id):
    conn = sqlite3.connect(DB)
    cursor = conn.cursor()
    if(table == 'QuestionPull'):
        cursor.execute('SELECT %s FROM %s WHERE id = %d'%(what, table, chat_id))
    elif(table == 'Instructors'):
        cursor.execute('SELECT chat_id from Instructors where FIO = %s'%(str(chat_id)))
    else:
        cursor.execute('SELECT %s FROM %s WHERE chat_id = %d'%(what, table, chat_id))
    
    exec = cursor.fetchall()
    cursor.close()
    conn.close()
    print(exec)
    return exec

def get_data_prepod(DB,table,what, chat_id):
    conn = sqlite3.connect(DB)
    cursor = conn.cursor()
    print('ИМЯ ФАМИЛИЯ В ОБРАЩЕНИИ', chat_id)
    cursor.execute('SELECT chat_id from Instructors where FIO = \'%s\''%(str(chat_id)))
    
    exec = cursor.fetchall()
    cursor.close()
    conn.close()
    print(exec)
    return exec

def get_data_instructor(DB,table,chat_id):
    conn = sqlite3.connect(DB)
    cursor = conn.cursor()
    if(table == 'QuestionPull'):
        cursor.execute('SELECT * FROM %s WHERE ( instructor_id = %d or student_id = %d) and stream = %d'%(table, chat_id, chat_id, True))
    exec = cursor.fetchall()
    
    cursor.close()
    conn.close()
    print(exec)
    return exec

def insert_q(DB,text,keywords,src,body):
    try:
        conn = sqlite3.connect(DB)
        cursor = conn.cursor()
        cursor.execute('select MAX(id) from QUESTION')
        exec = cursor.fetchall()
        print(exec)
        #print(time.)
        cursor.execute('INSERT INTO question(id, text, keywords, src, body) VALUES( %d,\'%s\', \'%s\',\'%s\', \'%s\');'%(int(exec[0][0]) + 1,str(text), str(keywords), str(src), str(body)))
        conn.commit()
        cursor.close()
        conn.close()
        return int(exec[0][0]) + 1
    except Exception as ex:
        print((str(ex)))


def insert_q_json(DB,text,keywords,url):
    try:
        conn = sqlite3.connect(DB)
        cursor = conn.cursor()
        cursor.execute('select MAX(id) from QUESTION')
        exec = cursor.fetchall()
        print(exec)
        #print(time.)
        cursor.execute('INSERT INTO question(id, text , keywords , src) VALUES( %d,\'%s\', \'%s\', \'%s\');'%(int(exec[0][0]) + 1,str(text), str(keywords), str(url)))
        conn.commit()
        cursor.close()
        conn.close()
        return int(exec[0][0]) + 1
    except Exception as ex:
        print((str(ex)))


def update_s(DB, table, where, what, id):#add some str data value to user's database row
    try:
        conn = sqlite3.connect(DB)
        print(what,id)
        cursor = conn.cursor()
        if(table == 'QuestionPull'):
            print(str(id))
            cursor.execute('UPDATE %s SET %s = \'%s\' WHERE  student_id = %d'%(table, where, what, int(id)))
        else:
            cursor.execute('UPDATE %s SET %s = \'%s\' WHERE  chat_id = %d'%(table, where, what, int(id)))
        conn.commit()
        cursor.close()
        conn.close()
        print('%s Updated'%(where))
    except Exception as ex:
        print(str(ex))
    

def update_i(DB, table, where, what, id):#add some int data value to user's database row
    try:
        conn = sqlite3.connect(DB)
        print(what,id)
        cursor = conn.cursor()
        if(table == 'QuestionPull'):
            print(str(id))
            cursor.execute('UPDATE %s SET %s = %d WHERE  student_id = %d'%(table, where, what, id))
        else:
            cursor.execute('UPDATE %s SET %s = %d WHERE  chat_id = %d'%(table, where, what, int(id)))
        conn.commit()
        cursor.close()
        conn.close()
        print('%s Updated'%(where))
    except Exception as ex:
        print(str(ex))


def getPeriodQuestionByData(DB, startData, endData):
    try:
        conn = sqlite3.connect(DB)
        cursor = conn.cursor()
        #cursor.execute('SELECT * FROM question WHERE date between \'%s\' and \'%s\''%(str(startData),str(endData)))
        cursor.execute('SELECT * FROM question WHERE date BETWEEN ' + '\''+ toSqlDataFormat(startData) + '\'' +  ' and '+ '\'' + toSqlDataFormat(endData) + '\'')
      
        exec = cursor.fetchall()
        cursor.close()
        conn.close()
        return exec
    except Exception as exec:
        return exec

        
def getQuestionByData(DB, Data):
    try:
        conn = sqlite3.connect(DB)
        cursor = conn.cursor()
        #cursor.execute('SELECT * FROM question WHERE date between \'%s\' and \'%s\''%(str(startData),str(endData)))
        cursor.execute('SELECT * FROM question WHERE date = '+ '\''+ toSqlDataFormat(Data) + '\'' )
      
        exec = cursor.fetchall()
        cursor.close()
        conn.close()
        return exec
    except Exception as exec:
        return exec


def toSqlDataFormat(string):
    print(string)
    dataForm = '20' + string[6] + string[7] + '-' + string[3] + string[4] + '-' + string[0] + string[1] 
    print(dataForm)
    return dataForm


def getKeywordsRows(kwString):
    kwArray = kwString.split()
    if len(kwArray) > 0:
        cast = 'select * from question where ( keywords like \''+ kwArray[0] + ' %\' OR keywords like \'% '+ kwArray[0] +' %\' OR keywords like \'% '+ kwArray[0] +'\')'
        if(len(kwArray) > 1):
            for kw in kwArray[1:]:
                cast = cast + ' AND ( keywords like \''+ kw + ' %\' OR keywords like \'% '+ kw +' %\' OR keywords like \'% '+ kw +'\')'      
    try:
        conn = sqlite3.connect('BK536')
        cursor = conn.cursor()
        cursor.execute(cast)
        exec = cursor.fetchall()
        cursor.close()
        conn.close()
        return exec
    except Exception as exec:
        return exec


def getAdrresToEchoTo(chat_id):
    try:
        conn = sqlite3.connect('BK536')
        cursor = conn.cursor()
        #cursor.execute('SELECT * FROM question WHERE date between \'%s\' and \'%s\''%(str(startData),str(endData)))
        cursor.execute('select * from QuestionPull where (instructor_id == %s or student_id == %s) and stream == %d'%(chat_id , chat_id, True) ) #Получаем 
        exec = cursor.fetchall()
        cursor.close()
        conn.close()
        return exec
    except Exception as exec:
        return exec
        
def pushMessageInHistory(chat_id,text):
    try:
        conn = sqlite3.connect('BK536')
        cursor = conn.cursor()
        print(text)
        cursor.execute('UPDATE  QuestionPull SET answer = answer || \'%s\'  || \'\n\' WHERE  student_id = %d'%(text, int(chat_id)))
        conn.commit()
        cursor.close()
        conn.close()
    except Exception as ex:
        print(str(ex))
    

#DB = 'BK536'
#table = 'users'
#print(get_all_data(DB,table))
#update_s(DB, table,'username','@Frugtoy','111')
#print(get_all_data(DB,table))

#print(toSqlDataFormat('22.06.21'))

#conn = sqlite3.connect('BK536')
#cursor = conn.cursor()
#cursor.execute('select * from question where date between ' + '\''+ toSqlDataFormat('22.06.00') + '\'' +  ' and '+ '\'' + toSqlDataFormat('22.06.42')+ '\'')
#exec = cursor.fetchall()
#cursor.close()
#conn.close()
#print(exec)
#print(getKeywordsRows('ГДЕ КОГДА'))

#
# print((getAdrresToEchoTo(1121500749)))