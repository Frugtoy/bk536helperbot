# BK536HELPERBOT

Бот - помощник для работы с информационными ресурсами кафедры разработан с целью увеличения возможных способов коммуникации студентов и преподавателей, а также автоматизации ответов на однотипные вопросы.

 ![img_2.png](img_2.png)

## Конфигурация
```Прописывается в файле .env```
~~~dotenv
API_TOKEN = "pass bot token here"  
HOST = "pass host here"  
USER = "pass database username here"  
PASSWORD = "pass database password here"  
DB_NAME = "pass db name here"  
PORT = "pass db port here" 
~~~

## Конфигурация v.2
```если .env с кавычками не кушает```

~~~dotenv 
API_TOKEN = pass bot token here  
HOST = pass host here  
USER = pass database username here  
PASSWORD = pass database password here  
DB_NAME = pass db name here  
PORT = pass db port here  
~~~

## Запуск программы

~~~bash
docker build -t helper ./
~~~

```ждем окончания билда...```

~~~bash
docker run --name helper --add-host PASS_HOST --restart unless-stopped -d helper
~~~  
```Для просмотра логов```
~~~bash
docker exec -it helper bash
~~~
