# start from base
FROM ubuntu:latest
MAINTAINER Boris Lesin <@Frugtoy>

# install system-wide deps for python and node
RUN apt-get update && \
    apt-get install -y python3 && \
    apt-get install -y python3-pip

# add app specific deps
ADD requirements.txt ./requirements.txt

# fetch app specific deps
RUN pip3 install -r requirements.txt

# copy our application code
ADD request_worker.py ./request_worker.py
ADD bot.py ./bot.py
ADD database_wrapper.py ./database_wrapper.py
#ADD config.py ./config.py
ADD check.py ./check.py
RUN mkdir -p users && chmod 777 users
ADD .env ./.env
WORKDIR ./

# expose port

# start app
CMD [ "/usr/bin/python3", "./bot.py" ]