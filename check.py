def isNumchar(i):
    flag = False
    if(i == '1' or 
    i == '2'    or 
    i == '3'    or 
    i == '4'    or 
    i == '5'    or 
    i == '6'    or
    i == '7'    or
    i == '8'    or
    i == '9'    or
    i == '0'    ):
        flag = True    
    return flag

def isValid(p):
    flag = False
    if(len(p)== 8):
        if(p[2]=='.' and p[5]=='.'):
            idx = 0
            for i in p:
                #print(i)
                if(idx != 2 and idx !=5):
                    flag = isNumchar(i)
                if(flag == False):
                    print("err:notdigit")
                    break
                idx = idx + 1
        else:
            print('err:dotlogic')
    else:
        print("err:size")
    print(flag)
    return flag


def instructorFilter(str, data):
    for i in data:
        if str == i:
            return False
    return True
